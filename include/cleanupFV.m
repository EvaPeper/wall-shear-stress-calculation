function [F_new,V_new,Vkeeptheseindices] = cleanupFV(F,V)
% CLEANUPFV removes duplicates vertices, faces 
%           and removes nan and inf entries from the vertices.
% 
%    1) removes duplicate vertices in V and adjusts F accordingly.
%    2) removes duplicate faces in F
%    3) removes nan and inf vertices in V and adjusts F accordingly.
%
%    [F_new,V_new] = cleanupFV(F,V)
%   
%    --- Author information
%    Wouter Potters
%    Academic Medical Center, Amsterdam, The Netherlands
%    w.v.potters@amc.nl
%    Date: 02-May-2013

% See also PATCH, ISOSURFACE
% if (nargin >= 3) && ( (ischar(varargin{1}) && strcmp(varargin{1},'debug')) || (varargin{1} == 1) )
%     debug = true;
% else
%     debug = false;
% end

if max(F(:)) > size(V,1)
%     disp('Biggest face index exceeds number of vertices, will remove impossible faces')
    remove = any(F > size(V,1),2);
    F_new = F(~remove,:);
else
    F_new = F;
end

%% merge identical vertices
[V_new, saveindices, indVnew] = unique(V,'rows');
switchind=[(1:length(V))' indVnew];
F_new = replace(F_new,switchind(:,1),switchind(:,2));

[~,~,indF_new] = unique(sort(F_new,2),'rows');
F_new = F_new(indF_new,:);

% if debug 
%     disp('------------------------------------')
%     disp('cleanupFV progress: ')
%     disp([num2str(size(V,1) - size(V_new,1)) ' duplicate vertices were deleted / merged.'])
% end

if (size(V,1) - size(V_new,1)) == 0
    V_new = V;
    F_new = F;
    indVnew = 1:length(V_new);
end

%% remove NaN and Inf from vertices
if any(any(isnan(V_new),2) | any(isinf(V_new),2)) % check if this is the case
    remove_these_vertices = find(any(isnan(V_new),2) | any(isinf(V_new),2)); % get the indices of the rows
    loopvars = sort(remove_these_vertices,'descend')';
    for irem_ = 1:length(loopvars)
        irem = loopvars(irem_);
        F_new(any(F_new == irem,2),:) = []; % remove the faces containing the removed vertex
        F_new(F_new > irem) = F_new(F_new > irem) - 1; % shift all higher indices 1 down
        V_new(irem,:) = []; % remove the vertex
        saveindices(irem,:) = []
    end % for
    
%     if debug
%         disp([num2str(numel(remove_these_vertices)) ' Nan or Inf rows were deleted from vertices'])
%     end
else
%     if debug
%         disp(['0 Nan or Inf rows were deleted from vertices'])
%     end
end % if

%% remove duplicate faces
[~,ic] = unique(sort(F_new,2),'rows'); % find unique rows
remove_faces = (~ismember(1:length(F_new),ic)); % Select faces to remove
F_new(remove_faces,:) = []; % actually removes faces

% if debug 
%     disp([num2str(sum(remove_faces)) ' duplicate faces were removed.'])
% end

%% delete unused vertices and adjust faces accordingly.
% find indices of old and new vertices
Fvals = unique(F_new(:));
old_indices_in_V  = (sort(Fvals,'ascend'));
new_indices_in_V  = (1:length(old_indices_in_V))';

% replace the vertex indices in the faces with the newly calculated indices
F_new    = replace(F_new, old_indices_in_V, new_indices_in_V);

% remove the unused vertices
V_new = V_new(old_indices_in_V,:);
saveindices = saveindices(old_indices_in_V,:);

Vkeeptheseindices = saveindices;
% if debug
%     disp([num2str(length(V) - length(V_new)) ' vertices were not used by the faces and thus removed.'])
%     disp('------------------------------------')
% end

%% Check if output is still valid
% if max(F_new(:)) > size(V_new,1)
%     error('Biggest face index exceeds number of vertices')
% end