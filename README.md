# README #

## EXAMPLE CODE
Please refer to **example_wallshearstress.m** for an example of wall shear stress calculation and visualisation.

## CALCULATE_WSS_3D.M
This is the main function where wall shear stress is calculated.

Inputs: 

  + velocity data (three-directional) and it's coordinates (x,y,z)
  + surface consisting of connected triangles (vertices + faces)
  + total distance and distance between individual points along the inward normal.
~~~~

 [shearstress_object,surface_faces,surface_vertices] = calculate_wss_3d( ...
      datax, datay, dataz,...               % coordinates x,y,z in meter
      velocity_x,velocity_y,velocity_z, ... % velocity components x,y,z in meter/s
      viscosity_medium,...                  % viscosity_medium 'water' or 'blood' 
      surface_faces,surface_vertices,...    % input surface elements/faces (triangles) and coordinates/vertices in mm
      length_inward_normal_in_m,...         % lenght of the inward normal that is used in meter (usually +/- 50% of the diameter)
      distance_per_point_in_m )             % distance per inward point; usually chosen as: ( (50% diameter) /3 )
  ---
  NOTE 1: UNITS OF INPUT DATA ARE VERY IMPORTANT 
  NOTE 2: Please make sure your segmentation is as accurate as possible
~~~~

## Literature
The method in 'calculate_wss_3d.m' is described in this article: 
  _Potters, W. V., van Ooij, P., Marquering, H., vanBavel, E. and Nederveen, A. J. (2014), **Volumetric arterial wall shear stress calculation based on cine phase contrast MRI.** J. Magn. Reson. Imaging. doi: 10.1002/jmri.24560_ - http://onlinelibrary.wiley.com/doi/10.1002/jmri.24560/full

See also this review article for other WSS calculation methods:
_Potters WV, Marquering HA, VanBavel E, Nederveen AJ. **Measuring Wall Shear Stress Using Velocity-Encoded MRI.** Curr Cardiovasc Imaging Rep 2014;7:9257. doi: 10.1007/s12410-014-9257-1_ - http://link.springer.com/article/10.1007/s12410-014-9257-1

## Screenshots of results
These figures are made using example_wallshearstress.m

Image of segmentation and 2 arbitrary slices of the magnitude image
![IMAGE](https://bitbucket.org/wpotters/wall-shear-stress-calculation/raw/master/fig1.png)

Image showing the wall shear stress magnitude as a coloured vessel surface
![IMAGE](https://bitbucket.org/wpotters/wall-shear-stress-calculation/raw/master/fig2.png)

Subselection of 3000 wall shear stress arrows
![IMAGE](https://bitbucket.org/wpotters/wall-shear-stress-calculation/raw/master/fig3.png)

Zoomed version of previous figure.
![IMAGE](https://bitbucket.org/wpotters/wall-shear-stress-calculation/raw/master/fig3_zoom.png)